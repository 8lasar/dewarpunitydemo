﻿using UnityEngine;
using System.Collections;

public class changeFOV : MonoBehaviour {
    public Camera left;
    public Camera right;

    float vfov;
    float aspect;
    float dispwidth;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        vfov = left.fieldOfView;
        aspect = left.aspect;
        dispwidth = Screen.width;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            vfov -= 0.05f;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            vfov += 0.05f;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            aspect -= 0.002f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            aspect += 0.002f;
        }
        left.fieldOfView = vfov;
        right.fieldOfView = vfov;
        left.aspect = aspect;
        right.aspect = aspect;
    }

    void OnGUI()
    {

        for (int i=0;i<=1;i++)
        {
            GUI.Button(new Rect(dispwidth / 2 * i, 0, 300, 50), "vfov:" + vfov.ToString() + " use left right");
            GUI.Button(new Rect(dispwidth / 2 * i, 50, 300, 50), "aspect:" + aspect.ToString() + " use up down");
        }

    }
}
