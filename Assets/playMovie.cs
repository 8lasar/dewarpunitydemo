﻿using UnityEngine;
using System.Collections;

public class playMovie : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // this line of code will make the Movie Texture begin playing

        MovieTexture mt = ((MovieTexture)GetComponent<Renderer>().material.mainTexture);
        mt.loop = true;
        mt.Play();

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
