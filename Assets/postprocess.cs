﻿using UnityEngine;
using System.Collections;

//input mouse pos rorate camera

//screen set resolution
public class postprocess : MonoBehaviour {

	public Material mat;


	static private float factor = 0.3f; //range 0.0 - 0.3

	private Vector4 distInfo = new Vector4(Screen.width, Screen.height, 1.0f / Screen.width, 1.0f / Screen.height);
	private Vector4 distFactor = new Vector4(factor, factor, 0.0f, 0.0f);
	private Vector3 lastLoc;
    private Vector3 campos;
    private bool dragging = false;
    private float speed = 0.1f;

	void Start () {

	}

	// Called by the camera to apply the image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		//Vector3 angles = Input.mousePosition - lastLoc;
		//transform.Rotate (new Vector3(angles.y * 0.2f, angles.x * -0.2f, 0.0f));
		//lastLoc = Input.mousePosition;
		//pass info to shader
		mat.SetVector ("distInfo", distInfo);
		mat.SetVector ("distFactor", distFactor);
		//mat is the material containing your shader
		Graphics.Blit(source,destination,mat);
	}

    void Update()
    {
        distInfo = new Vector4(Screen.width, Screen.height, 1.0f / Screen.width, 1.0f / Screen.height);

        if (Input.GetMouseButtonDown(0))
        {
            lastLoc = Input.mousePosition;
            campos = this.transform.rotation.eulerAngles;
            dragging = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
        }
        if (dragging)
        {
            Vector3 delta = (Input.mousePosition - lastLoc) * speed;

            Vector3 newpos = campos;
            newpos.y = campos.y - delta.x;
            newpos.x = campos.x + delta.y;
            //newpos.x = Mathf.Clamp(newpos.x, -45, 45);

            this.transform.rotation = Quaternion.Euler(newpos);
        }

    }

}
