﻿Shader "Custom/dewarp" { // defines the name of the shader 
	Properties {
		_MainTex ("_MainTex", 2D) = "white" {}
		distFrame ("distFrame", 2D) = "" {}
		distInfo ("distInfo", Vector) = (0,0,0,0)
		distFactor ("distFactor", Vector) = (0,0,0,0)
		
    }
	SubShader { // Unity chooses the subshader that fits the GPU best
		Pass { // some shaders require multiple passes
			GLSLPROGRAM // here begins the part in Unity's GLSL

			#ifdef VERTEX // here begins the vertex shader

			void main() {
				gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
				// this line transforms the predefined attribute 
				// gl_Vertex of type vec4 with the predefined
				// uniform gl_ModelViewProjectionMatrix of type mat4
				// and stores the result in the predefined output 
				// variable gl_Position of type vec4.
			}

			#endif // here ends the definition of the vertex shader
			#ifdef FRAGMENT // here begins the fragment shader

			uniform sampler2D _MainTex;
			uniform sampler2D distFrame;
			uniform vec4 distInfo;
			uniform vec4 distFactor;

			void main() {
				vec2 undistort = texture2D(distFrame, gl_FragCoord.xy * distInfo.zw * vec2(2.0,1.0)).gb * (distInfo.xy * distFactor.xy);

				//if (gl_FragCoord.x > distInfo.x || gl_FragCoord.y > distInfo.y) {
				if (undistort.x < 0.0001 && undistort.y < 0.0001) {
					gl_FragColor = vec4(0.0,0.0,0.0,0.0);
					return;
				}
				//vec2(51.0, 38.0)
				gl_FragColor = texture2D(_MainTex,
					//sample loc + undistortion - half distortion factor to NDC plus
					((gl_FragCoord.xy * (vec2(1.0,1.0) - distFactor.xy)) - undistort.xy + (distInfo.xy * distFactor.xy)) * distInfo.zw);
			}

			#endif // here ends the definition of the fragment shader

			ENDGLSL // here ends the part in GLSL 
		}
	}
}